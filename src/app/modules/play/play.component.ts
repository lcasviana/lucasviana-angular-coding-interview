import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ReplaySubject, Subscription } from 'rxjs';
import { switchMap, takeUntil, tap } from 'rxjs/operators';
import { QuestionsService } from '../../core/services/questions.service';
import { QuestionModel } from '../../core/state/question.model';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss'],
})
export class PlayComponent implements OnInit, OnDestroy {
  private readonly destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);

  done: boolean = false;
  correct: number = 0;
  incorrect: number = 0;

  questions$ = this.questionsService.questions$;
  gettingQuestions$ = this.questionsService.gettingQuestions$;
  getQuestionsSubscription: Subscription = this.route.queryParams
    .pipe(
      switchMap((params) =>
        this.questionsService.getQuestions({
          type: params.type,
          amount: params.amount,
          difficulty: params.difficulty,
        })
      )
    )
    .subscribe();

  constructor(
    private readonly route: ActivatedRoute,
    private readonly questionsService: QuestionsService
  ) {}

  ngOnInit(): void {
    this.questions$
      .pipe(takeUntil(this.destroyed$), tap(console.log))
      .subscribe((questions) => {
        this.done = questions.every(
          (question: any) => question.selectedId !== undefined
        );
        if (this.done) {
          this.correct = questions.filter((question: any) =>
            question.answers.find(
              (q: any) => q.isCorrect && q._id === question.selectedId
            )
          ).length;
          this.incorrect = questions.filter((question: any) =>
            question.answers.find(
              (q: any) => !q.isCorrect && q._id === question.selectedId
            )
          ).length;
        }
      });
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  onAnswerClicked(
    questionId: QuestionModel['_id'],
    answerSelected: string
  ): void {
    this.questionsService.selectAnswer(questionId, answerSelected);
  }
}
